This can be used as an alternative to the Terraform managed resource aws_elastic_beanstalk_application_version. This
script is meant to be used by a continuous deployment system (Bamboo, Jenkins, etc) and it should be called in two
places:

1. Separate task/stage that runs before Terraform
2. As a Terraform provisioner for the aws_elastic_beanstalk_application resource

This will ensure that the application version is created before Terraform creates or updates the Elastic Beanstalk
Environment. This also enables the Environment to be updated with one API call, instead of potentially two. On the
initial run, the script may fail if the Elastic Beanstalk Application has not been created yet. Because of this that
specific error is ignored by the script.

The following environment variables are used

  `TF_VAR_aws_region`
  
  `TF_VAR_elastic_beanstalk_s3_bucket`
  
  `TF_VAR_elastic_beanstalk_s3_key`
  
  `TF_VAR_elastic_beanstalk_application_name`
  
  `TF_VAR_elastic_beanstalk_application_version`


Example Terraform Document

```
variable "aws_region" { }
variable "elastic_beanstalk_application_name" { }
variable "elastic_beanstalk_application_version" { }

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_elastic_beanstalk_application" "default" {
  name        = "${var.elastic_beanstalk_application_name}"
  description = ""

  provisioner "local-exec" {
    command = "./create_application_version.py"
  }
}

resource "aws_elastic_beanstalk_environment" "default" {
  name                = ""
  application         = "${aws_elastic_beanstalk_application.default.name}"
  solution_stack_name = ""
  version_label       = "${var.elastic_beanstalk_application_version}"
}
```